<?php

namespace App\Http\Controllers\Admin;

use App\Mail\ActiveEmail;
use App\Mail\ResetPassword;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        if (Auth::check() === true){
            return redirect()->route('admin.home');
        }
        return view('admin.index');
    }

    public function home()
    {
        return view('admin.dashboard');
    }

    /** Login */
    public function login(Request $request)
    {
        if (in_array('', $request->only('email', 'password')))
        {
            $json['message'] = $this->message->error("Ops, informe todos os dados!")->render();
            return response()->json($json);
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            $json['message'] = $this->message->info('Por favor, informe um e-mail válido!')->render();
            return response()->json($json);
        }

        $credentials = [
            'email'     => $request->email,
            'password'  =>  $request->password
        ];
        if (!Auth::attempt($credentials)){
            $json['message'] = $this->message->info('Usuário ou senha incorreto!')->render();
            return response()->json($json);
        }else{
            $user = User::where('email', $request->email)->first();
            if (!empty($user['email_verified_at'])){
                $this->authenticated($request->getClientIp());
                $json['redirect'] = route('admin.home');
                return response()->json($json);
            }else{
                $json['message'] = $this->message->info('Por favor, verifique a sua caixa de email e valide o seu acesso.')->render();
                return response()->json($json);
            }
        }


    }

    /** Cadastro de usuário */
    public function register()
    {
        return view('admin.register');
    }

    public function registerDo(Request $request)
    {
        if (in_array('', $request->only('email', 'password', 'name')))
        {
            $json['message'] = $this->message->error("Ops, informe todos os dados!")->render();
            return response()->json($json);
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            $json['message'] = $this->message->info('Por favor, informe um e-mail válido!')->render();
            return response()->json($json);
        }

        if (strlen($request->name) < 3 ){
            $json['message'] = $this->message->info('Por favor, informe um nome válido!')->render();
            return response()->json($json);
        }

        $user = User::where('email', $request->email)->first();
        if (!empty($user)){
            $json['message'] = $this->message->info('Usuário já cadastrado!')->render();
            return response()->json($json);
        }

        User::create([
           'name'   =>  $request->name,
           'email'   =>  $request->email,
           'password'   =>  bcrypt($request->password),
        ]);

        $user = User::where('email', $request->email)->first();

        if (!empty($user)) {
            $token = Str::random(60);
            $user->forceFill([
                'remember_token' => $token
            ])->save();
            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'token' => $token,
            ];

            Mail::send(new ActiveEmail($data));
        }
        $json['message'] = $this->message->success('Usuário cadastrado, verifique a sua caixa de email para validar oo seu acesso!')->render();
        $json['redirect'] = route('admin.login');
        return response()->json($json);
    }

    public function activeEmail(Request $request)
    {
        $url = $request->url();
        $token = explode('/', $url)[5];
        $user = User::where('remember_token', $token)->first();

        if (!empty($user)){
            $user->forceFill([
                'email_verified_at' => date('Y-m-d H:i:s'),
            ])->save();

            Auth::login($user);

            return redirect('admin/home');
        }else{
            $json['message'] = $this->message->info("Ops, Token expirado!")->render();
            $json['redirect'] = route('admin.resetPassword');
            return response()->json($json);
        }
    }

    /** Logout */
    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }

    /** Último acesso e IP */
    public function authenticated(string $ip)
    {
        $user = User::where('id', Auth::user()->id);
        $user->update([
            'last_login_at' => date('Y-m-d H:i:s'),
            'last_login_ip' => $ip
        ]);
    }
}
