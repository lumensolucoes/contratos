<?php

namespace App\Http\Controllers\Admin;

use App\Mail\ResetPassword;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    public function resetPassword()
    {
        return view('admin.resetPassword');
    }

    public function resetPasswordDo(Request $request)
    {
        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            $json['message'] = $this->message->info('Por favor, informe um e-mail válido!')->render();
            return response()->json($json);
        }

        $user = User::where('email', $request->email)->first();
        if (!empty($user)){
            $token = Str::random(60);
            $user->forceFill([
                'remember_token' => $token
            ])->save();
            $data = [
                'name'      =>  $user->name,
                'email'     =>  $user->email,
                'token'     =>  $token,
            ];

            Mail::send(new ResetPassword($data));
        }else{
            $json['message'] = $this->message->info('Usuário não encontrado')->render();
            $json['redirect'] = route('admin.register');
            return response()->json($json);
        }
    }

    public function password()
    {
        return view('admin.password');
    }

    public function passwordDo(Request $request)
    {
        if (in_array('', $request->only('password', 'password_2')))
        {
            $json['message'] = $this->message->error("Ops, informe todos os dados!")->render();
            return response()->json($json);
        }

        if ($request->password !== $request->password_2){
            $json['message'] = $this->message->info("Ops, as senhas não conferem")->render();
            return response()->json($json);
        }

        $url = $request->session()->previousUrl();
        $token = explode('/', $url)[5];
        $user = User::where('remember_token', $token)->first();

        if (!empty($user)){
            $user->forceFill([
               'password'           => bcrypt($request->password),
               'email_verified_at' => date('Y-m-d H:i:s'),
            ])->save();

            Auth::login($user);
            $json['redirect'] = route('admin.home');
            return response()->json($json);
        }else{
            $json['message'] = $this->message->info("Ops, Token expirado!")->render();
            $json['redirect'] = route('admin.resetPassword');
            return response()->json($json);
        }
    }
}
