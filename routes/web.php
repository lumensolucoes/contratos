<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function (){

    Route::get('/', 'AuthController@showLoginForm')->name('login');

    Route::post('login', 'AuthController@login')->name('login.do');

    /** Cadastro de usuário */
    Route::get('cadastrar', 'AuthController@register')->name('register');
    Route::post('cadastrar', 'AuthController@registerDo')->name('register.do');

    /** Validação de e-mail */
    Route::get('active-email/{token}', 'AuthController@activeEmail')->name('active.email');

    /** Reset Password */
    Route::get('reset', 'ResetPasswordController@resetPassword')->name('resetPassword');
    Route::post('reset', 'ResetPasswordController@resetPasswordDo')->name('resetPassword.do');
    Route::get('reset/{token}', 'ResetPasswordController@password')->name('password');
    Route::post('reset/token', 'ResetPasswordController@passwordDo')->name('password.do');

    Route::group(['middleware' => ['auth']], function (){

        Route::get('/home', 'AuthController@home')->name('home');

        Route::get('/documentos', 'DocumentsController@index')->name('documents.index');


        //Route::get('users/team', 'UserController@team')->name('users.team');
        //Route::resource('users', 'UserController');
    });


    Route::get('logout', 'AuthController@logout')->name('logout');
});
