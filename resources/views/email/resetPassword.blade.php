@component('mail::message')
# Introduction
{{$name}}
-------------------
{{$email}}
The body of your message.

@component('mail::button', ['url' => $token])
Recuperar senha
@endcomponent
<p>Follow this link to set a new password: <a href="{{$token}}">{{$token}}</a>.  If you did not recently try to reset your password, ignore this email.</p>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
