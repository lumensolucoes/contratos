@component('mail::message')
#Olá, {{$name}}!

Seja bem vindo à {{ config('app.name') }}!

Confirme seu cadastro clicando no botão abaixo e comece a utilizar nossa plataforma de assinatura eletrônica e gestão de documentos.

@component('mail::button', ['url' => $token])
COMEÇAR AGORA!
@endcomponent

Obrigado,<br>
{{ config('app.name') }}

<p>Não compartilhe este e-mail<p>

<p>Para sua segurança, não encaminhe este e-mail para ninguém.</p>

<p>Sobre a <a href="#">{{ config('app.name') }}</a></p>

<p>A {{ config('app.name') }} é uma plataforma de assinatura eletrônica de documentos. A sua assinatura é registrada em um log auditável, e o seu documento é protegido por tranca eletrônica e criptografia.<br>

<a href="{{ config('app.url') }}">Dúvidas</a></p>
@endcomponent
